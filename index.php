<?php
session_start();
if(isset($_SESSION['user']) && $_SESSION['user_type']==='t')
{
?>
<!DOCTYPE html>
<html class="no-js">
    
    <head>
        <title>Teacher's Panel</title>
        <!-- Bootstrap -->
        <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
        <link href="bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" media="screen">
        <link href="vendors/easypiechart/jquery.easy-pie-chart.css" rel="stylesheet" media="screen">
        <link href="assets/styles.css" rel="stylesheet" media="screen">
        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
            <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
        <script src="vendors/modernizr-2.6.2-respond-1.1.0.min.js"></script>
    </head>
    
    <body>
        <div class="navbar navbar-fixed-top">
            <div class="navbar-inner">
                <div class="container-fluid">
                    <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse"> <span class="icon-bar"></span>
                     <span class="icon-bar"></span>
                     <span class="icon-bar"></span>
                    </a>
                    <a class="brand" href="#">Teacher's Panel</a>
                    <div class="nav-collapse collapse">
                        <ul class="nav pull-right">
                            <li class="dropdown">
                                <a href="#" role="button" class="dropdown-toggle" data-toggle="dropdown"> <i class="icon-user"></i> <?php echo $_SESSION['name'];?><i class="caret"></i>

                                </a>
                                <ul class="dropdown-menu">
                                    <!--<li>
                                        <a tabindex="-1" href="#">Edit Profile</a>
                                    </li>
                                    <li class="divider"></li>-->
                                    <li>
                                        <a tabindex="-1" href="logout.php">Logout</a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                      
                           
                            </li>
                            
                        </ul>
                    </div>
                    <!--/.nav-collapse -->
                </div>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row-fluid">
                <div class="span3" id="sidebar">
                    <ul class="nav nav-list bs-docs-sidenav nav-collapse collapse">
                        <li class="active">
                            <a href="index.html"><i class="icon-chevron-right"></i> Dashboard</a>
                        </li>
                        <li>
                            <a href="addcourse.php"><i class="icon-chevron-right"></i> Add New Course</a>
                        </li>
                        <li>
                            <a href="viewcourses.php"><i class="icon-chevron-right"></i> View Your Course</a>
                        </li>
                        
                    </ul>
                </div>
                
                <!--/span-->
                <div class="span9" id="content">
				<?php
				
								if(isset($_GET['id1']))
								{
								?>
									<br>
									<br>
									<div class="alert alert-success alert-block">
										<a class="close" data-dismiss="alert" href="#">&times;</a>
										<h4 class="alert-heading">Success!</h4>
										<?php echo $_GET['id1'];?>
										
									</div>
                        <?php
						}
						?>
                        <div class="container">
      <div class="header">
       
        
      </div>
</div>
<div class="col-md-6">    
	<div class="jumbotron">
        <h1>Profile</h1>
		<table class="table">
       <tr > 
	   <td rowspan="4">

		<img src="upload/<?php echo $_SESSION['image'];?>" width="100" height="100" alt="10" class="img-circle">
		</td>
<td>Name</td><td><?php echo $_SESSION['name'];?></td>

<tr><td>Email-Id<td><?php echo $_SESSION['user'];?>


		
		</table>
		
       </div>
	   </div>
	
                   
            <hr>
            <footer>
                <p></p>
            </footer>
        </div>
        <!--/.fluid-container-->
        <script src="vendors/jquery-1.9.1.min.js"></script>
        <script src="bootstrap/js/bootstrap.min.js"></script>
        <script src="vendors/easypiechart/jquery.easy-pie-chart.js"></script>
        <script src="assets/scripts.js"></script>
        <script>
        $(function() {
            // Easy pie charts
            $('.chart').easyPieChart({animate: 1000});
        });
        </script>
		
    </body>

</html>
<?php
}
else
if(isset($_SESSION['user']) && $_SESSION['user_type']==='s')
{
?>
<!DOCTYPE html>
<html class="no-js">
    
    <head>
        <title>Teacher's Panel</title>
        <!-- Bootstrap -->
        <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
        <link href="bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" media="screen">
        <link href="vendors/easypiechart/jquery.easy-pie-chart.css" rel="stylesheet" media="screen">
        <link href="assets/styles.css" rel="stylesheet" media="screen">
        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
            <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
        <script src="vendors/modernizr-2.6.2-respond-1.1.0.min.js"></script>
    </head>
    
    <body>
        <div class="navbar navbar-fixed-top">
            <div class="navbar-inner">
                <div class="container-fluid">
                    <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse"> <span class="icon-bar"></span>
                     <span class="icon-bar"></span>
                     <span class="icon-bar"></span>
                    </a>
                    <a class="brand" href="#">Student's Panel</a>
                    <div class="nav-collapse collapse">
                        <ul class="nav pull-right">
                            <li class="dropdown">
                                <a href="#" role="button" class="dropdown-toggle" data-toggle="dropdown"> <i class="icon-user"></i> <?php echo $_SESSION['name'];?><i class="caret"></i>

                                </a>
                                <ul class="dropdown-menu">
                                   <!-- <li>
                                        <a tabindex="-1" href="#">Edit Profile</a>
                                    </li>
                                    <li class="divider"></li>-->
                                    <li>
                                        <a tabindex="-1" href="logout.php">Logout</a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                      
                           
                            </li>
                            
                        </ul>
                    </div>
                    <!--/.nav-collapse -->
                </div>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row-fluid">
                <div class="span3" id="sidebar">
                    <ul class="nav nav-list bs-docs-sidenav nav-collapse collapse">
                        <li class="active">
                            <a href="index.html"><i class="icon-chevron-right"></i> Dashboard</a>
                        </li>
                        
                        <li>
                            <a href="viewcourses.php"><i class="icon-chevron-right"></i> View Courses</a>
                        </li>
                        
                    </ul>
                </div>
                
                <!--/span-->
                <div class="span9" id="content">
				<?php
				
								if(isset($_GET['id2']))
								{
								?>
									<br>
									<br>
									<div class="alert alert-success alert-block">
										<a class="close" data-dismiss="alert" href="#">&times;</a>
										<h4 class="alert-heading">Success!</h4>
										<?php echo $_GET['id2'];?>
										
									</div>
                        <?php
						}
						?>
                        <div class="container">
      <div class="header">
       
        
      </div>
</div>
<div class="col-md-6">    
	<div class="jumbotron">
        <h1>Profile</h1>
		<table class="table">
       <tr > 
	   <td rowspan="4">

		<img src="upload/<?php echo $_SESSION['image'];?>" width="100" height="100" alt="10" class="img-thumbnail">
		</td>
<td>Name</td><td><?php echo $_SESSION['name'];?></td>

<tr><td>Email-Id<td><?php echo $_SESSION['user'];?>


		
		</table>
		
       </div>
	   </div>
	
                   
            <hr>
            <footer>
                <p></p>
            </footer>
        </div>
        <!--/.fluid-container-->
        <script src="vendors/jquery-1.9.1.min.js"></script>
        <script src="bootstrap/js/bootstrap.min.js"></script>
        <script src="vendors/easypiechart/jquery.easy-pie-chart.js"></script>
        <script src="assets/scripts.js"></script>
        <script>
        $(function() {
            // Easy pie charts
            $('.chart').easyPieChart({animate: 1000});
        });
        </script>
		
    </body>

</html>
<?php
}
else
echo "<script type="."text/javascript".">location.href = 'home_page.php?message=Login With Proper Credentials to continue!';</script>";
?>