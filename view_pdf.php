<?php
session_start();
if(isset($_SESSION['user']) && $_SESSION['user_type']==='t')
{
?>

<!DOCTYPE html>
<html class="no-js">
    
    <head>
        <title>Teacher's Panel</title>
        <!-- Bootstrap -->
        <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
        <link href="bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" media="screen">
        <link href="vendors/easypiechart/jquery.easy-pie-chart.css" rel="stylesheet" media="screen">
        <link href="assets/styles.css" rel="stylesheet" media="screen">
        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
            <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
        <script src="vendors/modernizr-2.6.2-respond-1.1.0.min.js"></script>
    </head>
    
    <body>
        <div class="navbar navbar-fixed-top">
            <div class="navbar-inner">
                <div class="container-fluid">
                    <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse"> <span class="icon-bar"></span>
                     <span class="icon-bar"></span>
                     <span class="icon-bar"></span>
                    </a>
                    <a class="brand" href="#">Teacher's Panel</a>
                    <div class="nav-collapse collapse">
                        <ul class="nav pull-right">
                            <li class="dropdown">
                                <a href="#" role="button" class="dropdown-toggle" data-toggle="dropdown"> <i class="icon-user"></i> <?php echo $_SESSION['name'];?><i class="caret"></i>

                                </a>
                                <ul class="dropdown-menu">
                                    <li>
                                        <a tabindex="-1" href="#">Edit Profile</a>
                                    </li>
                                    <li class="divider"></li>
                                    <li>
                                        <a tabindex="-1" href="logout.php">Logout</a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                      
                           
                                
                            </li>
                            
                        </ul>
                    </div>
                    <!--/.nav-collapse -->
                </div>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row-fluid">
                <div class="span3" id="sidebar">
                    <ul class="nav nav-list bs-docs-sidenav nav-collapse collapse">
                        <li class="active">
                            <a href="index.php"><i class="icon-chevron-right"></i> Dashboard</a>
                        </li>
                        
                        <li>
                            <a href="addcourse.php"><i class="icon-chevron-right"></i> Add New Course</a>
                        </li>
                        <li>
                            <a href="viewcourses.php"><i class="icon-chevron-right"></i> View Your Course</a>
                        </li>
                        
                    </ul>
                </div>
                
                <!--/span-->
                <div class="span9" id="content">
		
									<br>
									<br>
									<div class="alert alert-warning alert-block">
										<a class="close" data-dismiss="alert" href="#">&times;</a>
										<h4 class="alert-heading">Note!</h4>
										If you are using Chrome and Video doesn't work.Type chrome:flags in the address bar and enable hardware-accelerated-video-codec!
										
									</div>
                        
						<?php


require_once "credentials.php";

$x1=$_POST['lesson1'];


$stmt = $dbh->prepare("select * from lessions where lesson_id='$x1'");
if($stmt->execute())
{
$r=$stmt->fetch();
$x=$r['file1'];
$temp = explode(".", $x);
$extension = end($temp);

if($extension=="mp4")
{
?>
<center><video   controls
 preload="auto" width="640" height="264" poster=""
 >
 <source src="upload/<?php echo $x;?>" type='video/mp4'>
 
</video></center>
<?php
}
else

if($extension=="pdf")
{
?>
<center>
<object data="upload/<?php echo $x;?>" type="application/pdf" width="100%" height="800">
<a href="upload/<?php echo $x;?>">test.pdf</a>
</object>
</center>
<?php
}
if($extension=="pptx")
{
?>
<center>
<object data="upload/<?php echo $x;?>" type="application/ppt" width="100%" height="800">
<a href="upload/<?php echo $x;?>">test.pdf</a>
</object>
</center>
<?php
}
}

?>

                        	
                   
            <hr>
            <footer>
                <p></p>
            </footer>
        </div>
        <!--/.fluid-container-->
        <script src="vendors/jquery-1.9.1.min.js"></script>
        <script src="bootstrap/js/bootstrap.min.js"></script>
        <script src="vendors/easypiechart/jquery.easy-pie-chart.js"></script>
        <script src="assets/scripts.js"></script>
        <script>
        $(function() {
            // Easy pie charts
            $('.chart').easyPieChart({animate: 1000});
        });
        </script>
    </body>

</html>
<?php
}
else
if(isset($_SESSION['user']) && $_SESSION['user_type']==='s')
{
?>
<!DOCTYPE html>
<html class="no-js">
    
    <head>
        <title>Student's Panel</title>
        <!-- Bootstrap -->
        <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
        <link href="bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" media="screen">
        <link href="vendors/easypiechart/jquery.easy-pie-chart.css" rel="stylesheet" media="screen">
        <link href="assets/styles.css" rel="stylesheet" media="screen">
        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
            <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
        <script src="vendors/modernizr-2.6.2-respond-1.1.0.min.js"></script>
    </head>
    
    <body>
        <div class="navbar navbar-fixed-top">
            <div class="navbar-inner">
                <div class="container-fluid">
                    <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse"> <span class="icon-bar"></span>
                     <span class="icon-bar"></span>
                     <span class="icon-bar"></span>
                    </a>
                    <a class="brand" href="#">Student's Panel</a>
                    <div class="nav-collapse collapse">
                        <ul class="nav pull-right">
                            <li class="dropdown">
                                <a href="#" role="button" class="dropdown-toggle" data-toggle="dropdown"> <i class="icon-user"></i> <?php echo $_SESSION['name'];?><i class="caret"></i>

                                </a>
                                <ul class="dropdown-menu">
                                    <li>
                                        <a tabindex="-1" href="#">Edit Profile</a>
                                    </li>
                                    <li class="divider"></li>
                                    <li>
                                        <a tabindex="-1" href="logout.php">Logout</a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                      
                           
                                
                            </li>
                            
                        </ul>
                    </div>
                    <!--/.nav-collapse -->
                </div>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row-fluid">
                <div class="span3" id="sidebar">
                    <ul class="nav nav-list bs-docs-sidenav nav-collapse collapse">
                        <li class="active">
                            <a href="index.php"><i class="icon-chevron-right"></i> Dashboard</a>
                        </li>
                       
                        
                        <li>
                            <a href="viewcourses.php"><i class="icon-chevron-right"></i> View Your Course</a>
                        </li>
                        
                    </ul>
                </div>
                
                <!--/span-->
                <div class="span9" id="content">
		
									<br>
									<br>
									<div class="alert alert-warning alert-block">
										<a class="close" data-dismiss="alert" href="#">&times;</a>
										<h4 class="alert-heading">Note!</h4>
										If you are using Chrome and Video doesn't work.Type chrome:flags in the address bar and enable hardware-accelerated-video-codec!
										
									</div>
                        
						<?php


require_once "credentials.php";

$x1=$_POST['lesson1'];


$stmt = $dbh->prepare("select * from lessions where lesson_id='$x1'");
if($stmt->execute())
{
$r=$stmt->fetch();
$x=$r['file1'];
$temp = explode(".", $x);
$extension = end($temp);

if($extension=="mp4")
{
?>
<center><video   controls
 preload="auto" width="640" height="264" poster=""
 >
 <source src="upload/<?php echo $x;?>" type='video/mp4'>
 
</video></center>
<?php
}
else

if($extension=="pdf")
{
?>
<center>
<object data="upload/<?php echo $x;?>" type="application/pdf" width="100%" height="800">
<a href="upload/<?php echo $x;?>">test.pdf</a>
</object>
</center>
<?php
}
if($extension=="pptx")
{
?>
<center>
<object data="upload/<?php echo $x;?>" type="application/ppt" width="100%" height="800">
<a href="upload/<?php echo $x;?>">test.pdf</a>
</object>
</center>
<?php
}
}

?>

                        	
                   
            <hr>
            <footer>
                <p></p>
            </footer>
        </div>
        <!--/.fluid-container-->
        <script src="vendors/jquery-1.9.1.min.js"></script>
        <script src="bootstrap/js/bootstrap.min.js"></script>
        <script src="vendors/easypiechart/jquery.easy-pie-chart.js"></script>
        <script src="assets/scripts.js"></script>
        <script>
        $(function() {
            // Easy pie charts
            $('.chart').easyPieChart({animate: 1000});
        });
        </script>
    </body>

</html>
<?php
}
else
echo "<script type="."text/javascript".">location.href = 'home_page.php?message=Login With Proper Credentials to continue!';</script>";
?>
