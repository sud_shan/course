<?php
session_start();

if(isset($_SESSION['user']) && $_SESSION['user_type']==='t')
{
require_once "credentials.php";
$userid=$_SESSION['user_id'];

$stmt = $dbh->prepare("select * from courses where trainer_id='$userid'");
$stmt->execute();
$stmt->setFetchMode(PDO::FETCH_BOTH);


?>
<!DOCTYPE html>
<html class="no-js">
    
    <head>
        <title>Teacher's Panel</title>
        <!-- Bootstrap -->
        <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
        <link href="bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" media="screen">
        <link href="vendors/easypiechart/jquery.easy-pie-chart.css" rel="stylesheet" media="screen">
        <link href="assets/styles.css" rel="stylesheet" media="screen">
		<link href="assets/DT_bootstrap.css" rel="stylesheet" media="screen">
        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
            <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
        <script src="vendors/modernizr-2.6.2-respond-1.1.0.min.js"></script>
    </head>
    
    <body>
        <div class="navbar navbar-fixed-top">
            <div class="navbar-inner">
                <div class="container-fluid">
                    <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse"> <span class="icon-bar"></span>
                     <span class="icon-bar"></span>
                     <span class="icon-bar"></span>
                    </a>
                    <a class="brand" href="#">Teacher's Panel</a>
                    <div class="nav-collapse collapse">
                        <ul class="nav pull-right">
                            <li class="dropdown">
                                <a href="#" role="button" class="dropdown-toggle" data-toggle="dropdown"> <i class="icon-user"></i> <?php echo $_SESSION['name'];?><i class="caret"></i>

                                </a>
                                <ul class="dropdown-menu">
                                    <!--<li>
                                        <a tabindex="-1" href="#">Edit Profile</a>
                                    </li>
                                    <li class="divider"></li>-->
                                    <li>
                                        <a tabindex="-1" href="logout.php">Logout</a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                      
                           
                                
                            </li>
                            
                        </ul>
                    </div>
                    <!--/.nav-collapse -->
                </div>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row-fluid">
                <div class="span3" id="sidebar">
                    <ul class="nav nav-list bs-docs-sidenav nav-collapse collapse">
                        <li class="active">
                            <a href="index.php"><i class="icon-chevron-right"></i> Dashboard</a>
                        </li>
                       
                        <li>
                            <a href="addcourse.php"><i class="icon-chevron-right"></i> Add New Course</a>
                        </li>
                        <li>
                            <a href="viewcourses.php"><i class="icon-chevron-right"></i> View Your Course</a>
                        </li>
                        
                    </ul>
                </div>
                
                <!--/span-->
                <div class="span9" id="content">
					<div class="alert alert-info alert-block">
										<a class="close" data-dismiss="alert" href="#">&times;</a>
										<h4 class="alert-heading">Info!</h4>
										Click on the Courses to Update it!
					</div>
				 <div class="block-content collapse in">
                                <div class="span12">
  									<table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="example">
										<thead>
											<tr>
												<th>Course Id</th>
												<th>Course Name</th>
												<th>Date of Creation</th>
												
											</tr>
										</thead>
										<tbody>
										<?php
											$odd==1;
											 
											while($r = $stmt->fetch()){
										   
												if($odd===1)
												{
												$odd==0;
												?>
												<tr class="odd gradeX" onclick="window.location='maintain_course.php?id=<?php echo $r[0];?>'">
													<td><?php echo $r['course_id'];?></td>
													<td><?php echo $r['course_name'];?></td>
													<td><?php echo $r['date_of_creation'];?></td>
													
												</tr>
												<?php
												}
												else
												{
												$odd=1;
												?>
												<tr class="even gradeX"  onclick="window.location='maintain_course.php?id=<?php echo $r[0];?>'">
													<td><?php echo $r['course_id'];?></td>
													<td><?php echo $r['course_name'];?></td>
													<td><?php echo $r['date_of_creation'];?></td>
													
												</tr>
												<?php
												}
											}
											?>
											
											
										</tbody>
									</table>
                                </div>
                            </div>
                        	
                   
            <hr>
            <footer>
                <p></p>
            </footer>
        </div>
        <!--/.fluid-container-->
        <script src="vendors/jquery-1.9.1.min.js"></script>
        <script src="bootstrap/js/bootstrap.min.js"></script>
        <script src="vendors/easypiechart/jquery.easy-pie-chart.js"></script>
        <script src="assets/scripts.js"></script>
		<script src="vendors/datatables/js/jquery.dataTables.min.js"></script>
		<script src="assets/DT_bootstrap.js"></script>
        <script>
        $(function() {
            
        });
        </script>
    </body>

</html>
<?php
}
else
if(isset($_SESSION['user']) && $_SESSION['user_type']==='s')
{
require_once "credentials.php";
$userid=$_SESSION['user_id'];

$stmt = $dbh->prepare("select * from courses ");
$stmt->execute();
$stmt->setFetchMode(PDO::FETCH_BOTH);


?>
<!DOCTYPE html>
<html class="no-js">
    
    <head>
        <title>Student's Panel</title>
        <!-- Bootstrap -->
        <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
        <link href="bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" media="screen">
        <link href="vendors/easypiechart/jquery.easy-pie-chart.css" rel="stylesheet" media="screen">
        <link href="assets/styles.css" rel="stylesheet" media="screen">
		<link href="assets/DT_bootstrap.css" rel="stylesheet" media="screen">
        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
            <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
        <script src="vendors/modernizr-2.6.2-respond-1.1.0.min.js"></script>
    </head>
    
    <body>
        <div class="navbar navbar-fixed-top">
            <div class="navbar-inner">
                <div class="container-fluid">
                    <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse"> <span class="icon-bar"></span>
                     <span class="icon-bar"></span>
                     <span class="icon-bar"></span>
                    </a>
                    <a class="brand" href="#">Student's Panel</a>
                    <div class="nav-collapse collapse">
                        <ul class="nav pull-right">
                            <li class="dropdown">
                                <a href="#" role="button" class="dropdown-toggle" data-toggle="dropdown"> <i class="icon-user"></i> <?php echo $_SESSION['name'];?><i class="caret"></i>

                                </a>
                                <ul class="dropdown-menu">
                                    <li>
                                        <a tabindex="-1" href="#">Edit Profile</a>
                                    </li>
                                    <li class="divider"></li>
                                    <li>
                                        <a tabindex="-1" href="logout.php">Logout</a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                      
                           
                                
                            </li>
                            
                        </ul>
                    </div>
                    <!--/.nav-collapse -->
                </div>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row-fluid">
                <div class="span3" id="sidebar">
                    <ul class="nav nav-list bs-docs-sidenav nav-collapse collapse">
                        <li class="active">
                            <a href="index.php"><i class="icon-chevron-right"></i> Dashboard</a>
                        </li>
                       
                        
                        <li>
                            <a href="viewcourses.php"><i class="icon-chevron-right"></i> View  Courses</a>
                        </li>
                        
                    </ul>
                </div>
                
                <!--/span-->
                <div class="span9" id="content">
					<div class="alert alert-info alert-block">
										<a class="close" data-dismiss="alert" href="#">&times;</a>
										<h4 class="alert-heading">Info!</h4>
										Click on the View It!.
					</div>
				 <div class="block-content collapse in">
                                <div class="span12">
  									<table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="example">
										<thead>
											<tr>
												<th>Course Id</th>
												<th>Course Name</th>
												<th>Date of Creation</th>
												
											</tr>
										</thead>
										<tbody>
										<?php
											$odd==1;
											 
											while($r = $stmt->fetch()){
										   
												if($odd===1)
												{
												$odd==0;
												?>
												<tr class="odd gradeX" onclick="window.location='maintain_course.php?id=<?php echo $r[0];?>'">
													<td><?php echo $r['course_id'];?></td>
													<td><?php echo $r['course_name'];?></td>
													<td><?php echo $r['date_of_creation'];?></td>
													
												</tr>
												<?php
												}
												else
												{
												$odd=1;
												?>
												<tr class="even gradeX"  onclick="window.location='maintain_course.php?id=<?php echo $r[0];?>'">
													<td><?php echo $r['course_id'];?></td>
													<td><?php echo $r['course_name'];?></td>
													<td><?php echo $r['date_of_creation'];?></td>
													
												</tr>
												<?php
												}
											}
											?>
											
											
										</tbody>
									</table>
                                </div>
                            </div>
                        	
                   
            <hr>
            <footer>
                <p></p>
            </footer>
        </div>
        <!--/.fluid-container-->
        <script src="vendors/jquery-1.9.1.min.js"></script>
        <script src="bootstrap/js/bootstrap.min.js"></script>
        <script src="vendors/easypiechart/jquery.easy-pie-chart.js"></script>
        <script src="assets/scripts.js"></script>
		<script src="vendors/datatables/js/jquery.dataTables.min.js"></script>
		<script src="assets/DT_bootstrap.js"></script>
        <script>
        $(function() {
            
        });
        </script>
    </body>

</html>
<?php
}else
echo "<script type="."text/javascript".">location.href = 'home_page.php?message=Login With Proper Credentials to continue!';</script>";

?>