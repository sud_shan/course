<?php
$host="localhost";
$dbname="course_portal";
$user="root";
$pass="";
//$dbh = new PDO('mysql:host=localhost;dbname=course_portal',$user,$pass);
try {  
  
  
  # MySQL with PDO_MYSQL  
  $dbh = new PDO("mysql:host=$host;dbname=$dbname", $user, $pass);  
  $dbh->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
   
}  
catch(PDOException $e) {  
    echo $e->getMessage();  
}  
?>