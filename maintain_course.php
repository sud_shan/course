<?php
session_start();
if(isset($_SESSION['user']) && $_SESSION['user_type']==='t')
{


	require_once "credentials.php";
	if(isset($_GET['id']))
		$id=$_GET['id'];
	else
		$id=0;
	$stmt = $dbh->prepare("select * from courses where course_id='$id'");
	$stmt->execute();
	$stmt->setFetchMode(PDO::FETCH_BOTH);
	$r = $stmt->fetch();
	$stmt1 = $dbh->prepare("select * from sections where course_id='$id' order by section_number");
	$stmt1->execute();
	$stmt1->setFetchMode(PDO::FETCH_BOTH);


	?>
	<!DOCTYPE html>
	<html class="no-js">

	<head>
		<title>Teacher's Panel</title>
		<!-- Bootstrap -->
		<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
		<link href="bootstrap/css/docs.css" rel="stylesheet" media="screen">
		<link href="bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" media="screen">
		<link href="vendors/easypiechart/jquery.easy-pie-chart.css" rel="stylesheet" media="screen">
		<link href="assets/styles.css" rel="stylesheet" media="screen">
		<!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
			<!--[if lt IE 9]>
				<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
				<![endif]-->
				<script src="vendors/modernizr-2.6.2-respond-1.1.0.min.js"></script>
			</head>

			<body>
				<div class="navbar navbar-fixed-top">
					<div class="navbar-inner">
						<div class="container-fluid">
							<a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse"> <span class="icon-bar"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
							</a>
							<a class="brand" href="#">Teacher's Panel</a>
							<div class="nav-collapse collapse">
								<ul class="nav pull-right">
									<li class="dropdown">
										<a href="#" role="button" class="dropdown-toggle" data-toggle="dropdown"> <i class="icon-user"></i> <?php echo $_SESSION['name'];?><i class="caret"></i>

										</a>
										<ul class="dropdown-menu">
											
											<li class="divider"></li>
											<li>
												<a tabindex="-1" href="logout.php">Logout</a>
											</li>
										</ul>
									</li>
								</ul>


								<ul class="dropdown-menu">
									<li>
										<a tabindex="-1" href="#">Blog</a>
									</li>
									<li>
										<a tabindex="-1" href="#">News</a>
									</li>
									<li>
										<a tabindex="-1" href="#">Custom Pages</a>
									</li>
									<li>
										<a tabindex="-1" href="#">Calendar</a>
									</li>
									<li class="divider"></li>
									<li>
										<a tabindex="-1" href="#">FAQ</a>
									</li>
								</ul>
							</li>

						</ul>
					</div>
					<!--/.nav-collapse -->
				</div>
			</div>
		</div>
		<div class="container-fluid">
			<div class="row-fluid">
				<div class="span3" id="sidebar">
					<ul class="nav nav-list bs-docs-sidenav nav-collapse collapse">
						<li class="active">
							<a href="index.php"><i class="icon-chevron-right"></i> Dashboard</a>
						</li>

						<li>
							<a href="addcourse.php"><i class="icon-chevron-right"></i> Add New Course</a>
						</li>
						<li>
							<a href="viewcourses.php"><i class="icon-chevron-right"></i> View Your Course</a>
						</li>

					</ul>
				</div>
				<!--/span-->
				<div class="span9" id="content">
					<div class="span9" id="content">
						<?php

						if(isset($_GET['message']))
						{
							?>
							<br>
							<br>
							<div class="alert alert-success alert-block">
								<a class="close" data-dismiss="alert" href="#">&times;</a>
								<h4 class="alert-heading">Success!</h4>
								<?php echo $_GET['message'];?>

							</div>
							<?php
						}
						?>
						<div class="block-content collapse in">
							<div class="row-fluid padd-bottom">
								<div class="span3">
									<a href="#" class="thumbnail">
										<img data-src="holder.js/260x180" alt="" style="width: 260px; height: 180px;" src="upload/<?php echo $r['course_pic'];?>">
									</a>

								</div>
								<div class="span6">
									<h1><?php echo $r[1];?></h1>

								</div>
							</div>
						</div>
						<div class="block">
							<div class="navbar navbar-inner block-header">
								<div class="muted pull-left">Contents</div>
							</div>
							<table>
								<?php
								while($r1 = $stmt1->fetch())
								{
									$section=$r1[0];
									$stmt2 = $dbh->prepare("select * from lessions where section_id='$section'");
									$stmt2->execute();
									$stmt2->setFetchMode(PDO::FETCH_BOTH);

									?>
									<tr><td><h3><?php echo $r1[1];?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</h3></td><td><a href="#edit<?php echo $r1[0];?>" data-toggle="modal" class="btn  btn-primary ">Edit<i class="icon-pencil icon-white"></i></a>
									</td><td>
									<a href="#myModal<?php echo $r1[0];?>" data-toggle="modal" class="btn  btn-warning ">Add Lesson</a>
								</td>
								<td>
									<a href="#delete<?php echo $r1[0];?>" data-toggle="modal" class="btn  btn-danger">Delete</a>
								</td>
								<?php while($r2=$stmt2->fetch())
								{
									?>
									<tr border="1"><td>&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $r2[1];?>&nbsp;&nbsp;&nbsp;&nbsp;</td><td><a href="#myAlert<?php echo $r2[0];?>" data-toggle="modal" class="btn  btn-danger btn-mini"><i class="icon-remove icon-white"></i></a></td><td>
										<form action="view_pdf.php" method="post">
											<input type="hidden" name="lesson1" value="<?php echo $r2[0];?>"><br>
											<button onclick="submit" class="btn"><i class="icon-eye-open"></i>
											</button>
										</form>
									</td>
									<div id="myAlert<?php echo $r2[0];?>" class="modal hide">
										<div class="modal-header">
											<button data-dismiss="modal" class="close" type="button">&times;</button>
											<h3>Are You Sure?</h3>
										</div>
										<div class="modal-body">
											<form action="delete_lesson.php" method="post">
												<input type="hidden" name="course_id" value="<?php echo $id;?>">
												<input type="text" name="lesson" value="<?php echo $r2[0];?>">

											</div>
											<div class="modal-footer">
												<button class="btn btn-primary" type="submit">Confirm</button></form></td></tr>

											</div>
										</div>
										
										<?php }?>
										<div id="delete<?php echo $r1[0];?>" class="modal hide">
											<div class="modal-header">
												<button data-dismiss="modal" class="close" type="button">&times;</button>
												<h3>Are You Sure?</h3>
											</div>
											<div class="modal-body">
												<form action="delete_section.php" method="post">
													<input type="hidden" name="course_id" value="<?php echo $id;?>">
													<input type="text" name="section" value="<?php echo $r1[0];?>">

												</div>
												<div class="modal-footer">
													<button class="btn btn-primary" type="submit">Confirm</button></form></td></tr>
													
												</div>
											</div>
											<div id="edit<?php echo $r1[0];?>" class="modal hide">
												<div class="modal-header">
													<button data-dismiss="modal" class="close" type="button">&times;</button>
													<h3>Edit Section</h3>
												</div>

												
												<div class="modal-body">

															<form class="form-horizontal" action="update_section.php" method="post"enctype="multipart/form-data">
																<fieldset>

																	<div class="control-group">
																		<label class="control-label" for="typeahead" >Section Name</label>
																		<div class="controls">
																			<input type="text" class="span6"  name="section_name"  value="<?php echo $r1['section_name'];?>" required>
																			
																			<input type="hidden" name="course_id" value="<?php echo $id;?>" >
																		</div>
																	</div>
																	<div class="control-group">
																		<label class="control-label" for="typeahead" >Section Name</label>
																		<div class="controls">
																			
																			<input type="hidden" name="section_id" value="<?php echo $r1[0];?>">
																			
																		</div>
																	</div>
																	<div class="control-group">
																		<label class="control-label" for="typeahead" >Section Name</label>
																		<div class="controls">																		
																			
																			<input type="hidden" name="course_id" value="<?php echo $id;?>" >
																		</div>
																	</div>

																	<center><button type="submit" class="btn btn-primary">Save changes</button></center>
																</fieldset>
																</form>

															</div>
														</div>
													</div>
												</div>
												<div id="myModal<?php echo $r1[0];?>" class="modal hide">
													<div class="modal-header">
														<button data-dismiss="modal" class="close" type="button">&times;</button>
														<h3>Add a New Lesson</h3>
													</div>

													<div class="modal-body">
														<div class="block-content collapse in">
															<div class="span12">
																<form class="form-horizontal" action="add_lession.php" method="post"enctype="multipart/form-data">
																	<fieldset>

																		<div class="control-group">
																			<label class="control-label" for="typeahead" >Lesson Name</label>
																			<div class="controls">
																				<input type="text" class="span6" id="typeahead" name="lesson_name" data-provide="typeahead" data-items="4" data-source='' required>

																				<p class="help-block"></p>
																			</div>
																		</div>
																		<div class="control-group">
																			<label class="control-label" for="typeahead" >Lesson Number</label>
																			<div class="controls">
																				<input type="number" class="span6" id="typeahead" name="lesson_number" data-provide="typeahead" data-items="4" data-source='' required>

																				<p class="help-block"></p>
																				<div class="control-group">
																					<label class="control-label" for="multiSelect">Type of File</label>
																					<div class="controls">
																						<select id="select01" name="file_type"class="chzn-select">
																							<option value="video">Video</option><option value="pdf">Pdf</option><option value="ppt">PPt</option>
																						</select>
																						<p class="help-block"></p>
																					</div>

																				</div>
																				Upload<br>
																				<input type="file" name="file1">
																			</div><!-- /input-group -->
																		</div><!-- /.col-lg-6 -->
																	</div><!-- /.row -->
																</div>
															</div>
															<input type="hidden" name="section_id" value="<?php echo $r1[0];?>">
															<input type="hidden" name="course_id" value="<?php echo $id;?>">
															<center><button type="submit" class="btn btn-primary">Save changes</button></center>
														</form>

													</div>
												</div>
											</div>
										</div>

										<?php
									}?>
									

								</span>

							</table><br><br>

							<a href="#myModal" data-toggle="modal" class="btn btn-large btn-block btn-primary">Create a New Section</a>
							<div id="myModal" class="modal hide">
								<div class="modal-header">
									<button data-dismiss="modal" class="close" type="button">&times;</button>
									<h3>Create a New Section</h3>
								</div>
								<div class="modal-body">
									<div class="block-content collapse in">
										<div class="span12">
											<form class="form-horizontal" action="add_section.php" method="post">
												<fieldset>

													<div class="control-group">
														<label class="control-label" for="typeahead" >Section Name</label>
														<div class="controls">
															<input type="text" class="span6" id="typeahead" name="sec_name" data-provide="typeahead" data-items="4" data-source='' required>

															<p class="help-block"></p>
														</div>
													</div>
													<div class="control-group">
														<label class="control-label" for="typeahead" >Section Number</label>
														<div class="controls">
															<input type="number" class="span6" id="typeahead" name="sec_number" data-provide="typeahead" data-items="4" data-source='' required>

															<p class="help-block"></p>
														</div>
													</div>
													<input type="hidden" name="course_id" value="<?php echo $id;?>">

													<center><button type="submit" class="btn btn-primary">Save changes</button></center>


												</div>
											</div>
										</div>
									</div>
									
								</div>
								<div id="disqus_thread"></div>
								<script type="text/javascript">
									/* * * CONFIGURATION VARIABLES: EDIT BEFORE PASTING INTO YOUR WEBPAGE * * */
									var disqus_shortname = 'moneywizard'; // required: replace example with your forum shortname

									/* * * DON'T EDIT BELOW THIS LINE * * */
									(function() {
									var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = true;
									dsq.src = '//' + disqus_shortname + '.disqus.com/embed.js';
									(document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
								})();
							</script>
							<noscript>Please enable JavaScript to view the <a href="http://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
							<a href="http://disqus.com" class="dsq-brlink">comments powered by <span class="logo-disqus">Disqus</span></a>



							<hr>
							<footer>
								<p></p>
							</footer>
						</div>
						<!--/.fluid-container-->
						<script src="vendors/jquery-1.9.1.min.js"></script>
						<script src="bootstrap/js/bootstrap.min.js"></script>
						<script src="vendors/easypiechart/jquery.easy-pie-chart.js"></script>
						<script src="assets/scripts.js"></script>
						<script>
							$(function() {
							// Easy pie charts
							$('.chart').easyPieChart({animate: 1000});
						});
					</script>
				</body>

				</html>
				<?php
			}
			else
				if(isset($_SESSION['user']) && $_SESSION['user_type']==='s')
				{

					require_once "credentials.php";
					if(isset($_GET['id']))
						$id=$_GET['id'];
					else
						$id=0;
					$stmt = $dbh->prepare("select * from courses where course_id='$id'");
					$stmt->execute();
					$stmt->setFetchMode(PDO::FETCH_BOTH);
					$r = $stmt->fetch();
					$stmt1 = $dbh->prepare("select * from sections where course_id='$id' order by section_number");
					$stmt1->execute();
					$stmt1->setFetchMode(PDO::FETCH_BOTH);


					?>
					<!DOCTYPE html>
					<html class="no-js">

					<head>
						<title>Student's Panel</title>
						<!-- Bootstrap -->
						<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
						<link href="bootstrap/css/docs.css" rel="stylesheet" media="screen">
						<link href="bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" media="screen">
						<link href="vendors/easypiechart/jquery.easy-pie-chart.css" rel="stylesheet" media="screen">
						<link href="assets/styles.css" rel="stylesheet" media="screen">
						<!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
						<!--[if lt IE 9]>
						<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
						<![endif]-->
						<script src="vendors/modernizr-2.6.2-respond-1.1.0.min.js"></script>
					</head>

					<body>
						<div class="navbar navbar-fixed-top">
							<div class="navbar-inner">
								<div class="container-fluid">
									<a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse"> <span class="icon-bar"></span>
										<span class="icon-bar"></span>
										<span class="icon-bar"></span>
									</a>
									<a class="brand" href="#">Student's Panel</a>
									<div class="nav-collapse collapse">
										<ul class="nav pull-right">
											<li class="dropdown">
												<a href="#" role="button" class="dropdown-toggle" data-toggle="dropdown"> <i class="icon-user"></i> <?php echo $_SESSION['name'];?><i class="caret"></i>

												</a>
												<ul class="dropdown-menu">
													
													<li class="divider"></li>
													<li>
														<a tabindex="-1" href="logout.php">Logout</a>
													</li>
												</ul>
											</li>
										</ul>



									</li>

								</ul>
							</div>
							<!--/.nav-collapse -->
						</div>
					</div>
				</div>
				<div class="container-fluid">
					<div class="row-fluid">
						<div class="span3" id="sidebar">
							<ul class="nav nav-list bs-docs-sidenav nav-collapse collapse">
								<li class="active">
									<a href="index.php"><i class="icon-chevron-right"></i> Dashboard</a>
								</li>


								<li>
									<a href="viewcourses.php"><i class="icon-chevron-right"></i> View Your Course</a>
								</li>

							</ul>
						</div>

						<!--/span-->
						<div class="span9" id="content">
							<div class="span9" id="content">
								<?php

								if(isset($_GET['message']))
								{
									?>
									<br>
									<br>
									<div class="alert alert-success alert-block">
										<a class="close" data-dismiss="alert" href="#">&times;</a>
										<h4 class="alert-heading">Success!</h4>
										<?php echo $_GET['message'];?>

									</div>
									<?php
								}
								?>
								<div class="block-content collapse in">
									<div class="row-fluid padd-bottom">
										<div class="span3">
											<a href="#" class="thumbnail">
												<img data-src="holder.js/260x180" alt="" style="width: 260px; height: 180px;" src="upload/<?php echo $r['course_pic'];?>">
											</a>

										</div>
										<div class="span6">
											<h1><?php echo $r[1];?></h1>

										</div>
									</div>
								</div>
								<div class="block">
									<div class="navbar navbar-inner block-header">
										<div class="muted pull-left">Contents</div>
									</div>
									<table>
										<?php
										while($r1 = $stmt1->fetch())
										{
											$section=$r1[0];
											$stmt2 = $dbh->prepare("select * from lessions where section_id='$section'");
											$stmt2->execute();
											$stmt2->setFetchMode(PDO::FETCH_BOTH);

											?>
											<tr><td><h3><?php echo $r1[1];?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</h3></td>


												<?php while($r2=$stmt2->fetch())
												{
													?>
													<tr border="1"><td>&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $r2[1];?>&nbsp;&nbsp;&nbsp;&nbsp;</td><td><td>
														<form action="view_pdf.php" method="post">
															<input type="hidden" name="lesson1" value="<?php echo $r2[0];?>"><br>
															<button onclick="submit" class="btn"><i class="icon-eye-open"></i>
															</button>


															<?php }?>


															<?php
														}?>


													</span>

												</table>
												<br><br>



											</div>



											<div id="disqus_thread"></div>
											<script type="text/javascript">
												/* * * CONFIGURATION VARIABLES: EDIT BEFORE PASTING INTO YOUR WEBPAGE * * */
												var disqus_shortname = 'moneywizard'; // required: replace example with your forum shortname

												/* * * DON'T EDIT BELOW THIS LINE * * */
												(function() {
												var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = true;
												dsq.src = '//' + disqus_shortname + '.disqus.com/embed.js';
												(document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
											})();
										</script>
										<noscript>Please enable JavaScript to view the <a href="http://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
										<a href="http://disqus.com" class="dsq-brlink">comments powered by <span class="logo-disqus">Disqus</span></a>



										<hr>
										<footer>
											<p></p>
										</footer>
									</div>
									<!--/.fluid-container-->
									<script src="vendors/jquery-1.9.1.min.js"></script>
									<script src="bootstrap/js/bootstrap.min.js"></script>
									<script src="vendors/easypiechart/jquery.easy-pie-chart.js"></script>
									<script src="assets/scripts.js"></script>
									<script>
										$(function() {
										// Easy pie charts
										$('.chart').easyPieChart({animate: 1000});
									});
								</script>
							</body>

							</html>
							<?php
						}
						else
							echo "<script type="."text/javascript".">location.href = 'home_page.php?message=Login With Proper Credentials to continue!';</script>";

						?>