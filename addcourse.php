<?php
session_start();

if(isset($_SESSION['user']) && $_SESSION['user_type']==='t')
{
?>
<!DOCTYPE html>
<html class="no-js">
    
    <head>
        <title>Teacher's Panel</title>
        <!-- Bootstrap -->
        <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
        <link href="bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" media="screen">
        <link href="vendors/easypiechart/jquery.easy-pie-chart.css" rel="stylesheet" media="screen">
        <link href="assets/styles.css" rel="stylesheet" media="screen">
        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
            <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
        <script src="vendors/modernizr-2.6.2-respond-1.1.0.min.js"></script>
    </head>
    
    <body>
        <div class="navbar navbar-fixed-top">
            <div class="navbar-inner">
                <div class="container-fluid">
                    <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse"> <span class="icon-bar"></span>
                     <span class="icon-bar"></span>
                     <span class="icon-bar"></span>
                    </a>
                    <a class="brand" href="#">Teacher's Panel</a>
                    <div class="nav-collapse collapse">
                        <ul class="nav pull-right">
                            <li class="dropdown">
                                <a href="#" role="button" class="dropdown-toggle" data-toggle="dropdown"> <i class="icon-user"></i> <?php echo $_SESSION['name'];?><i class="caret"></i>

                                </a>
                                <ul class="dropdown-menu">
                                    <!--<li>
                                        <a tabindex="-1" href="#">Edit Profile</a>
                                    </li>
                                    <li class="divider"></li>-->
                                    <li>
                                        <a tabindex="-1" href="logout.php">Logout</a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                      
                           
                            </li>
                            
                        </ul>
                    </div>
                    <!--/.nav-collapse -->
                </div>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row-fluid">
                <div class="span3" id="sidebar">
                    <ul class="nav nav-list bs-docs-sidenav nav-collapse collapse">
                        <li class="active">
                            <a href="index.php"><i class="icon-chevron-right"></i> Dashboard</a>
                        </li>
                       
                        <li>
                            <a href="addcourse.php"><i class="icon-chevron-right"></i> Add New Course</a>
                        </li>
                        <li>
                            <a href="viewcourses.php"><i class="icon-chevron-right"></i> View Your Course</a>
                        </li>
                        
                    </ul>
                </div>
                
                <!--/span-->
				<div class="span9" id="content">
                <div class="row-fluid">
                        <!-- block -->
                        <div class="block">
                            <div class="navbar navbar-inner block-header">
                                <div class="muted pull-left">Add New Course</div>
                            </div>
                            <div class="block-content collapse in">
                                <div class="span12">
                                    <form class="form-horizontal" action="addcourse1.php" method="post" enctype="multipart/form-data">
                                      <fieldset>
                                        
                                        <div class="control-group">
                                          <label class="control-label" for="typeahead" >Course Title</label>
                                          <div class="controls">
                                            <input type="text" class="span6" id="typeahead" name="course_name" data-provide="typeahead" data-items="4" data-source='' required>
                                           
											<p class="help-block">Start typing to activate auto complete!</p>
                                          </div>
                                        </div>
										<div class="control-group">
                                          <label class="control-label" for="multiSelect">Select Domain</label>
                                          <div class="controls">
                                            <select multiple="multiple" id="multiSelect" name="domain[]" class="chzn-select span4" required>
                                              <option>Alabama</option><option>Alaska</option><option>Arizona</option><option>
                                            </select>
                                            <p class="help-block">Start typing to activate auto complete!</p>
                                          </div>
										  <div class="control-group">
                                          <label class="control-label" for="typeahead" >COurse Pic</label>
                                          <div class="controls">
                                            <input type="file" class="span6" id="typeahead" name="course_pic"  required>
                                           
											<p class="help-block"></p>
                                          </div>
                                        </div>
                                        
                                        <div class="form-actions">
                                          <button type="submit" class="btn btn-primary">Save changes</button>

                                        </div>
                                      </fieldset>
                                    </form>

                                </div>
                            </div>
                        </div>
                        <!-- /block -->
                    </div>

            <hr>
            <footer>
                <p></p>
            </footer>
        </div>
        <!--/.fluid-container-->
		<link href="vendors/datepicker.css" rel="stylesheet" media="screen">
        <link href="vendors/uniform.default.css" rel="stylesheet" media="screen">
        <link href="vendors/chosen.min.css" rel="stylesheet" media="screen">

        <link href="vendors/wysiwyg/bootstrap-wysihtml5.css" rel="stylesheet" media="screen">

        <script src="vendors/jquery-1.9.1.js"></script>
        <script src="bootstrap/js/bootstrap.min.js"></script>
        <script src="vendors/jquery.uniform.min.js"></script>
        <script src="vendors/chosen.jquery.min.js"></script>
        <script src="vendors/bootstrap-datepicker.js"></script>

        <script src="vendors/wysiwyg/wysihtml5-0.3.0.js"></script>
        <script src="vendors/wysiwyg/bootstrap-wysihtml5.js"></script>

        <script src="vendors/wizard/jquery.bootstrap.wizard.min.js"></script>

		<script src="vendors/easypiechart/jquery.easy-pie-chart.js"></script>
        <script src="assets/scripts.js"></script>
        <script src="assets/scripts.js"></script>
        <script>
        $(function() {
            $(".datepicker").datepicker();
            $(".uniform_on").uniform();
            $(".chzn-select").chosen();
            $('.textarea').wysihtml5();

            $('#rootwizard').bootstrapWizard({onTabShow: function(tab, navigation, index) {
                var $total = navigation.find('li').length;
                var $current = index+1;
                var $percent = ($current/$total) * 100;
                $('#rootwizard').find('.bar').css({width:$percent+'%'});
                // If it's the last tab then hide the last button and show the finish instead
                if($current >= $total) {
                    $('#rootwizard').find('.pager .next').hide();
                    $('#rootwizard').find('.pager .finish').show();
                    $('#rootwizard').find('.pager .finish').removeClass('disabled');
                } else {
                    $('#rootwizard').find('.pager .next').show();
                    $('#rootwizard').find('.pager .finish').hide();
                }
            }});
            $('#rootwizard .finish').click(function() {
                alert('Finished!, Starting over!');
                $('#rootwizard').find("a[href*='tab1']").trigger('click');
            });
        });
        </script>
        
       
        
        
    </body>

</html>
<?php
}
else
echo "<script type="."text/javascript".">location.href = 'home_page.php?message=Login With Proper Credentials to continue!';</script>";?>